package com.example.cuncis.sqlitedemo3.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cuncis.sqlitedemo3.R;
import com.example.cuncis.sqlitedemo3.adapter.MyContactAdapter;
import com.example.cuncis.sqlitedemo3.database.DatabaseHelper;
import com.example.cuncis.sqlitedemo3.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Contact> contactList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MyContactAdapter mAdapter;

    EditText etName, etPhone;
    Button btnSave, btnDisplay;

    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rv_contact);
        mAdapter = new MyContactAdapter(contactList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);

        db = new DatabaseHelper(this);

        //inserting contact
        Log.d("Insert", "onCreate: Inserting..");
        db.addContact(new Contact("Karanggg", "083854775376"));

        //Reading all contact
        List<Contact> contacts = db.getAllContact();

        for (Contact con: contacts) {
            String log = "Id: "+ con.getId() + ", Name: " + con.getName() +", Phone: "+ con.getPhoneNumber();
            Log.d("Name: ", log);
        }

        prepareContactList();

        btnDisplay = findViewById(R.id.btn_display);
        btnDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListContactActivity.class);
                startActivity(intent);
            }
        });

    }

    private void prepareContactList() {
        etName = findViewById(R.id.et_name);
        etPhone = findViewById(R.id.et_phone_number);
        btnSave = findViewById(R.id.btn_save);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getText().toString();
                String phone = etPhone.getText().toString();
                db.addContact(new Contact(name, phone));
                contactList.add(new Contact(name, phone));
                Toast.makeText(MainActivity.this, "Data inserted!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}














