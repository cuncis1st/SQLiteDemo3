package com.example.cuncis.sqlitedemo3.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cuncis.sqlitedemo3.R;
import com.example.cuncis.sqlitedemo3.model.Contact;

import java.util.List;

public class MyContactAdapter extends RecyclerView.Adapter<MyContactAdapter.MyContactViewHolder> {

    private List<Contact> listContact;

    public MyContactAdapter(List<Contact> listContact) {
        this.listContact = listContact;
    }

    @Override
    public MyContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_contact, null);
        return new MyContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyContactViewHolder holder, int position) {
        Contact contact = listContact.get(position);
        holder.tvName.setText(contact.getName());
        holder.tvPhone.setText(contact.getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return listContact != null ? listContact.size() : 0;
    }

    public class MyContactViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvPhone;
        ImageView img;

        public MyContactViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_list_name);
            tvPhone = itemView.findViewById(R.id.tv_list_phone);
            img = itemView.findViewById(R.id.img_list_contact);
        }
    }
}
